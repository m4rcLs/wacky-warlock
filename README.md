# wacky-warlock

**Wacky Warlock** is a fan website made for the TTRPG **Shadow of the Weird Wizard by Rob J. Schwalb**.  
It is designed to provide tools, aids and resources to enhance your SotWW games.

## Developing

1. Install dependencies
  
```sh
cd wacky-warlock
npm install
```
2. Start the development server:
 
```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version:

```bash
npm run build
```
You can preview the production build with `npm run preview`.