import type { RollResult, RollType } from "$lib/dice-roller/roll";
import { derived, writable } from "svelte/store";

const HISTORY_MAX_LENGTH = 100;

type Attributes = {
  Strength: number;
  Agility: number;
  Intellect: number;
  Will: number;
};

function loadScoreFromLocalStorage(attributeName: string): number {
  let score = 10;

  try {
    const key = `DiceRoller_${attributeName}`;
    if (localStorage.getItem(key)) {
      const storedScore = localStorage.getItem(key);
      score = storedScore ? parseInt(storedScore) : score;
    }
  } catch (e) {
    console.error(e);
  }

  return score;
}

function loadRollHistoryFromLocalStorage(): HistoricRollResult[] {
  try {
    const key = "DiceRoller_RollHistory";
    if (localStorage.getItem(key)) {
      const storedHistory = localStorage.getItem(key);
      if (!storedHistory) {
        return [];
      }
      const parsedHistory = JSON.parse(storedHistory);
      parsedHistory.forEach(
        (result: HistoricRollResult) =>
          (result.timestamp = new Date(result.timestamp))
      );
      return parsedHistory;
    }
  } catch (e) {
    console.error(e);
  }

  return [];
}
export const attributeScores = writable<Attributes>({
  Strength: loadScoreFromLocalStorage("Strength"),
  Agility: loadScoreFromLocalStorage("Agility"),
  Intellect: loadScoreFromLocalStorage("Intellect"),
  Will: loadScoreFromLocalStorage("Will"),
});
export const rollerDialogOpen = writable(false);

export const currentRoll = writable<RollResult | null>(null);
type HistoricRollResult = RollResult & { timestamp: Date };
export const rollHistory = writable<HistoricRollResult[]>(
  loadRollHistoryFromLocalStorage()
);

export const reversedRollHistory = derived(rollHistory, ($rollHistory) =>
  $rollHistory.toReversed()
);

export function storeRollInHistory(rollResult: RollResult) {
  rollHistory.update((currentHistory) => {
    //
    if (currentHistory.length === HISTORY_MAX_LENGTH) {
      currentHistory.shift();
    }
    currentHistory.push({
      ...rollResult,
      timestamp: new Date(),
    });
    persistRollHistory(currentHistory);
    return currentHistory;
  });
}

function persistRollHistory(history: HistoricRollResult[]) {
  try {
    const key = "DiceRoller_RollHistory";
    localStorage.setItem(key, JSON.stringify(history));
  } catch (e) {
    console.error(e);
  }
}

export function setupRoll(rollType: RollType, title: string, bonus: number) {
  rollerDialogOpen.set(true);
  currentRoll.set({
    title,
    bonus,
    rollType,
  });
}
