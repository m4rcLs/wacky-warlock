enum DieSize {
  Six = 6,
  Twenty = 20,
}

enum DieType {
  Attribute = 1,
  Boon,
  Bane,
  Luck,
  Damage,
}

export enum RollType {
  Attribute = 1,
  Luck,
  Damage,
}

type Die = {
  size: DieSize;
  type: DieType;
};

export type DieResult = {
  dieSize: DieSize;
  dieType: DieType;
  result: number;
  bonus?: number;
};

export type RollResult = {
  title: string;
  rollType: RollType;
  bonus?: number;
  result?: number;
  rolledDice?: DieResult[];
};

function rollDie(size: number) {
  return Math.floor(Math.random() * size) + 1;
}
function roll(die: Die, bonus: number, boonsOrBanes: number): DieResult[] {
  let result: DieResult[] = [
    {
      dieSize: die.size,
      dieType: die.type,
      bonus: bonus,
      result: rollDie(die.size),
    },
  ];

  const isBoon = boonsOrBanes > 0;
  for (let i = 0; i < Math.abs(boonsOrBanes); i++) {
    result.push({
      dieSize: DieSize.Six,
      dieType: isBoon ? DieType.Boon : DieType.Bane,
      result: rollDie(6),
    });
  }

  return result;
}

export function rollAttribute(
  bonus: number,
  boonsOrBanes: number
): DieResult[] {
  return roll(
    { size: DieSize.Twenty, type: DieType.Attribute },
    bonus,
    boonsOrBanes
  );
}

export function rollDamage(damageDice: number): DieResult[] {
  const result: DieResult[] = new Array<DieResult>(damageDice);
  for (let i = 0; i < damageDice; ++i) {
    result[i] = {
      dieSize: DieSize.Six,
      dieType: DieType.Damage,
      result: rollDie(6),
    };
  }

  return result;
}

export function rollLuck(boonsOrBanes: number): DieResult[] {
  return roll({ size: DieSize.Twenty, type: DieType.Luck }, 0, boonsOrBanes);
}

export function isAttributeRoll(die: DieResult): boolean {
  return die.dieType === DieType.Attribute;
}

export function isBoon(die: DieResult): boolean {
  return die.dieType === DieType.Boon;
}

export function isBane(die: DieResult): boolean {
  return die.dieType === DieType.Bane;
}

export function findHighestBoon(dieResults: DieResult[]): DieResult {
  return dieResults
    .filter((die) => isBoon(die))
    .reduce(
      (highest, current) => {
        if (current.result > highest.result) {
          return current;
        }
        return highest;
      },
      { dieSize: DieSize.Six, dieType: DieType.Boon, result: 0 }
    );
}

export function findHighestBane(dieResults: DieResult[]): DieResult {
  return dieResults
    .filter((die) => isBane(die))
    .reduce(
      (highest, current) => {
        if (current.result > highest.result) {
          return current;
        }
        return highest;
      },
      { dieSize: DieSize.Six, dieType: DieType.Bane, result: 0 }
    );
}

export function calculateResult(dieResults: DieResult[]): number {
  let result = 0;
  dieResults.forEach((dieResult) => {
    if (
      dieResult.dieType !== DieType.Boon &&
      dieResult.dieType !== DieType.Bane
    ) {
      result += dieResult.result;
      result += dieResult.bonus ? dieResult.bonus : 0;
    }
    return result;
  });

  const highestBoon = findHighestBoon(dieResults);
  const highestBane = findHighestBane(dieResults);

  result += highestBoon.result;
  result -= highestBane.result;

  return result;
}

export function createResultTooltip(roll: RollResult): string {
  let tooltip = "";
  const rolledDice = roll.rolledDice || [];

  if (roll.rollType === RollType.Damage) {
    return roll.rolledDice && roll.rolledDice.length > 0
      ? `${roll.rolledDice.length}d6: ` +
          roll.rolledDice?.map((dieResult) => dieResult.result).join(" + ")
      : "";
  }

  const attributeRoll = rolledDice.find((die) => isAttributeRoll(die));
  if (attributeRoll) {
    tooltip += `${attributeRoll.result} (d20) `;
    if (attributeRoll.bonus && attributeRoll.bonus !== 0) {
      tooltip +=
        " " +
        (attributeRoll.bonus > 0 ? "+ " : "- ") +
        Math.abs(attributeRoll.bonus) +
        " (Attribute Modifier)";
    }
  }
  const boon = findHighestBoon(rolledDice);
  const bane = findHighestBane(rolledDice);
  if (boon.result > 0) {
    tooltip += ` + ${boon.result} (Highest Boon)`;
  }

  if (bane.result > 0) {
    tooltip += ` - ${bane.result} (Highest Bane)`;
  }
  return tooltip;
}
