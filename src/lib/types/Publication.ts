import type { Base } from ".";
import type { PathTier } from "./Path";

type Publication = {
  name: string;
  description: string;
  publisher: string;
  link: string;
  pageCount: number;
  isThirdParty: boolean;
};

type Adventure = Publication & {
  tier: PathTier;
};
