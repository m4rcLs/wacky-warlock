import type { Base, SourceBook } from ".";

enum PathType {
  Battle = "Battle",
  Faith = "Faith",
  Power = "Power",
  Skill = "Skill",
}

export enum PathTier {
  Novice = "Novice",
  Expert = "Expert",
  Master = "Master",
}

type Path = Base & {
  name: string;
  description: string;
  tier: PathTier;
  type: PathType;
};
