import type { Base } from ".";

enum EnemyType {
  Ancestry = "Ancestry",
  Angel = "Angel",
  Beast = "Beast",
  Creation = "Creation",
  Demon = "Demon",
  Elemental = "Elemental",
  Faerie = "Faerie",
  Fiend = "Fiend",
  FungusPlant = "Fungus/Plant",
  Monster = "Monster",
  Spirit = "Spirit",
  Undead = "Undead",
  Amphibious = "(Amphibious)",
  Aquatic = "(Aquatic)",
  Swarm = "(Swarm)",
}
type Enemy = Base & {
  name: string;
  level: number;
  types: EnemyType[];
  solitary: boolean;
};
