export enum SourceBook {
  SOTWW = "Shadow of the Weird Wizard",
  SEOTWW = "Secrets of the Weird Wizard",
}

export type Base = {
  isThirdParty: boolean;
  page: number;
  source: SourceBook;
};
