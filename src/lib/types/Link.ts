enum LinkCategory {
  ActualPlay = "Actual Play",
  Podcast = "Podcast",
  Review = "Review",
  Social = "Social",
  Blog = "Blog",
  CharacterSheet = "Character Sheet",
}

type Link = {
  text: string;
  description: string;
  category: LinkCategory;
  url: string;
};
