import type { SourceBook } from ".";

enum RuleTypeEnum {
  Affliction,
  Attribute,
  Roll,
  Health,
  HarmfulEffect,
  Distance,
  Size,
  Movement,
  Sense,
  Knowledge,
  Communication,
  Companion,
  Combat,
  Action,
  Reaction,
  Attack,
  Spell,
  Chase,
}

type Rule = {
  title: string;
  subtitle: string;
  text: string;
  page: number;
  type: RuleTypeEnum;
  sourceBook: SourceBook;
};
