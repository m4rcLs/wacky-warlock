import type { Base, SourceBook } from ".";

enum Tradition {
  Aeromancy = "Aeromancy",
  Alchemy = "Alchemy",
  Alteration = "Alteration",
  Animism = "Animism",
  Astromancy = "Astromancy",
  Chaos = "Chaos",
  Chronomancy = "Chronomancy",
  Conjuration = "Conjuration",
  Cryomancy = "Cryomancy",
  DarkArts = "Dark Arts",
  Destruction = "Destruction",
  Divination = "Divination",
  Eldritch = "Eldritch",
  Enchantment = "Enchantment",
  Evocation = "Evocation",
  Geomancy = "Geomancy",
  Hydromancy = "Hydromancy",
  Illusion = "Illusion",
  Invocation = "Invocation",
  Necromancy = "Necromancy",
  Oneiromancy = "Oneiromancy",
  Order = "Order",
  Primal = "Primal",
  Protection = "Protection",
  Psionics = "Psionics",
  Pyromancy = "Pyromancy",
  Shadowmancy = "Shadowmancy",
  Skullduggery = "Skullduggery",
  Spiritualism = "Spiritualism",
  Symbolism = "Symbolism",
  Technomancy = "Technomancy",
  Teleportation = "Teleportation",
  War = "War",
}

enum SpellTier {
  Novice = "Novice",
  Expert = "Expert",
  Master = "Master",
}

type Spell = Base & {
  name: string;
  tradition: Tradition;
  tier: SpellTier;
  castings: number;
  text: string;
};

type Talent = Base & {
  name: string;
  tradition: Tradition;
  text: string;
};
