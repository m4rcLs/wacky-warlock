import type { SourceBook } from ".";

type Ancestry = {
  name: string;
  page: number;
  sourceBook: SourceBook;
  isThirdParty: boolean;
};
